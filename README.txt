Module: SEG
Author: Sean Dietrich <http://drupal.org/u/sean_e_dietrich>


Description
====================================================
Adds the SEG tracking system to your website.

Requirements
====================================================
* SEG user account

Optional Support
====================================================
* Commerce

Page specific tracking
====================================================
The default is set to "Add to every page except the listed pages". By
default the following pages are listed for exclusion:

/admin
/admin/*
/batch
/node/add*
/node/*/*
/user/*/*

These defaults are changeable by the website administrator or any other
user with 'Administer SEGAPP Settings' permission.

Like the blocks visibility settings in Drupal core, there is a choice for
"Add if the following PHP code returns TRUE." Sample PHP snippets that can be
used in this textarea can be found on the handbook page "Overview-approach to
block visibility" at http://drupal.org/node/64135.

Node specific tracking
====================================================
Every node has the ability to add it's on set of tags which are located in the 
node edit form. These tags are used when viewing the node.

Commerce tracking
====================================================
Orders and Carts are tracked with the products that are added to the cart.

User Opt-Out tracking
====================================================
With the "Opt-in or out of tracking" permission users can choose whether or not 
they are tracked.

Role tracking
====================================================
The default is to track all roles. Additionally, individual roles can be tracked
and leaving other roles excluded.
