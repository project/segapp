<?php

/**
 * @file
 * Administration page callbacks for the segapp module.
 */

/**
 * Implements hook_admin_settings() for module settings configuration.
 */
function segapp_admin_settings_form($form_state) 
{

    $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    );

    $form['account']['segapp_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug Calls to Seg'),
    '#default_value' => variable_get('segapp_debug', false)
    );

    $form['account']['segapp_account'] = array(
    '#title' => t('Website ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('segapp_account', ''),
    '#required' => true,
    '#description' => t('This ID is unique to each site you want to track separately.'),
    );

    $form['page_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page Visibility Settings'),
    );

    // Page specific visibility configurations.
    $visibility = variable_get('segapp_visibility_pages', 0);
    $pages = variable_get('segapp_pages', SEGAPP_PAGES);

    $form['page_vis_settings']['segapp_visibility_pages'] = array(
    '#type' => 'radios',
    '#title' => t('Add tracking to specific pages'),
    '#options' => array(
      t('Every page except the listed pages'),
      t('The listed pages only'),
    ),
    '#default_value' => $visibility,
    );

    $form['page_vis_settings']['segapp_pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#title_display' => 'invisible',
    '#default_value' => $pages,
    '#description' => t(
        "Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array(
        '%blog' => 'blog',
        '%blog-wildcard' => 'blog/*',
        '%front' => '<front>',
        )
    ),
    '#rows' => 10,
    );

    // Custom Metrics.
    $form['page_vis_settings']['segapp_page_tags'] = array(
    '#theme' => 'segapp_admin_custom_tags_table',
    '#title' => t('Custom Page Tags'),
    '#tree' => true,
    '#type' => 'fieldset',
    );

    $segapp_page_tags = variable_get('segapp_page_tags', array());
    $count = count($segapp_page_tags) < 10 ? 10 : count($segapp_page_tags) + 5;
    for ($i = 0; $i < $count; $i++) {
        $form['page_vis_settings']['segapp_page_tags'][$i]['page'] = array(
        '#title' => t('Page URL'),
        '#title_display' => 'invisible',
        '#type' => 'textfield',
        '#default_value' => isset($segapp_page_tags[$i]['page']) ? $segapp_page_tags[$i]['page'] : '',
        '#description' => t("Specify pages by using their paths. The '*' character is a wildcard.<br/>Example paths are blog for the blog page and blog/* for every personal blog. <front> is the front page."),
        );
        $form['page_vis_settings']['segapp_page_tags'][$i]['tags'] = array(
          '#default_value' => isset($segapp_page_tags[$i]['tags']) ? $segapp_page_tags[$i]['tags'] : '',
          '#description' => t('Tags that should be applied to the following page. Multiple tags can be entered in comma delimited'),
          '#maxlength' => 255,
          '#title' => t('Page Tags'),
          '#title_display' => 'invisible',
          '#type' => 'textfield',
          '#attributes' => array(
          'class' => array('tags-autocomplete'),
        ),
        );
    }

    $form['role_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Role Visibility Settings'),
    );
    $role_options = array_map('check_plain', user_roles());
    $form['role_vis_settings']['segapp_roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#default_value' => variable_get('segapp_roles', array()),
    '#options' => $role_options,
    '#description' => t('If none of the roles are selected, all users will be tracked. If a user has any of the roles checked, that user will be tracked (or excluded, depending on the setting above).'),
    );

    // Standard tracking configurations.
    $form['user_vis_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Users'),
    );
    $t_permission = array('%permission' => t('opt-in or out of tracking'));
    $form['user_vis_settings']['segapp_custom'] = array(
    '#type' => 'radios',
    '#title' => t('Allow users to customize tracking on their account page'),
    '#options' => array(
      t('No customization allowed'),
      t('Tracking on by default, users with %permission permission can opt out', $t_permission),
      t('Tracking off by default, users with %permission permission can opt in', $t_permission),
    ),
    '#default_value' => variable_get('segapp_custom', 0),
    );

    $form['user_vis_settings']['user_data']['segapp_fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Field Mapping'),
    '#description' => t('Map the fields located in the User account to the field in SEG. Email is automatically mapped and does not appear in list.'),
    '#tree' => true,
    );

    $options = segapp_get_user_fields();

    $form['user_vis_settings']['user_data']['segapp_fields']['mapping'] = array(
    // Theme this part of the form as a table.
    '#theme' => 'segapp_admin_settings_fields',
    // Pass header information to the theme function.
    '#header' => array(t('Drupal Field'), t('SEG Field'), t('Delete')),
    // Rows in the form table.
    'rows' => array(
      // Make it a tree for easier traversing of the entered values on
      // submission.
      '#tree' => true,
    ),
    );

    $fields = variable_get('segapp_fields', array());
    foreach ($fields as $key => $value) {
        $form['user_vis_settings']['user_data']['segapp_fields']['mapping']['rows'][$key]['drupal_field'] = array(
        '#title' => 'Drupal Field',
        '#title_display' => 'invisible',
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => $key,
        '#disabled' => true,
        );
        $form['user_vis_settings']['user_data']['segapp_fields']['mapping']['rows'][$key]['seg_field'] = array(
          '#title' => 'SEG Field',
          '#title_display' => 'invisible',
          '#type' => 'textfield',
          '#disabled' => true,
          '#default_value' => $value,
        );
        $form['user_vis_settings']['user_data']['segapp_fields']['mapping']['rows'][$key]['delete'] = array(
          '#title' => 'Delete',
          '#title_display' => 'invisible',
          '#type' => 'checkbox',
        );
    }

    $form['user_vis_settings']['user_data']['segapp_fields']['mapping']['rows']['new']['drupal_field'] = array(
    '#title' => 'Drupal Field',
    '#title_display' => 'invisible',
    '#type' => 'select',
    '#options' => $options,
    );
    $form['user_vis_settings']['user_data']['segapp_fields']['mapping']['rows']['new']['seg_field'] = array(
    '#title' => 'SEG Field',
    '#title_display' => 'invisible',
    '#type' => 'textfield',
    );
    $form['user_vis_settings']['user_data']['segapp_fields']['mapping']['rows']['new']['delete'] = array(
    '#type' => 'markup',
    );

    $form['#submit'][] = 'segapp_admin_settings_form_submit';
    $form['#validate'][] = 'segapp_admin_settings_form_validate';

    $form['#attached'] = array(
    'library' => array(
      array('system', 'ui.autocomplete'),
    ),
    'js' => array(
      'vertical-tabs' => drupal_get_path('module', 'segapp') . '/js/segapp.js',
    ),
    );

    $form['user_vis_settings']['user_data']['initialize_user'] = array(
    '#type' => 'fieldset',
    '#title' => t('Initialize User Mapping'),
    '#description' => t('Use variables within the URL to map to fields within SEG and start a user session.'),
    '#tree' => true,
    '#theme' => 'segapp_admin_custom_user_variables_table',
    );

    $segapp_initialize_tags = variable_get('segapp_url_initialize_tags', array());
    $count = count($segapp_initialize_tags) < 10 ? 10 : count($segapp_initialize_tags) + 5;
    for ($i = 0; $i < $count; $i++) {
        $form['user_vis_settings']['user_data']['initialize_user'][$i]['param'] = array(
        '#title' => t('URL Param'),
        '#title_display' => 'invisible',
        '#type' => 'textfield',
        '#default_value' => isset($segapp_initialize_tags[$i]['param']) ? $segapp_initialize_tags[$i]['param'] : '',
        '#description' => t("URL Param to capture data from."),
        );
        $form['user_vis_settings']['user_data']['initialize_user'][$i]['seg_field'] = array(
          '#default_value' => isset($segapp_initialize_tags[$i]['seg_field']) ? $segapp_initialize_tags[$i]['seg_field'] : '',
          '#description' => t('SEG Field to map data to.'),
          '#maxlength' => 255,
          '#title' => t('SEG Field'),
          '#title_display' => 'invisible',
          '#type' => 'textfield',
        );
    }

    return system_settings_form($form);
}

/**
 * Implements _form_validate().
 */
function segapp_admin_settings_form_validate($form, &$form_state) 
{
    $values = $form_state['values'];
    if (!empty($values['segapp_fields']['mapping']['rows']['new']['drupal_field']) && empty($values['segapp_fields']['mapping']['rows']['new']['seg_field'])) {
        form_set_error('', t('SEG Field Cannot Be Empty'));
    }

    if (!empty($values['segapp_fields']['mapping']['rows']['new']['drupal_field'])) {
        $drupal_field = $values['segapp_fields']['mapping']['rows']['new']['drupal_field'];
        if (isset($values['segapp_fields']['mapping']['rows'][$drupal_field]['drupal_field']) && $values['segapp_fields']['mapping']['rows'][$drupal_field]['delete'] == 0) {
            form_set_error('', t('Drupal Field already being used.'));
        }
    }
}

/**
 * Implements _form_submit().
 */
function segapp_admin_settings_form_submit($form, &$form_state) 
{
    $values = $form_state['values'];
    $mapped_values = array();

    $tmp_values = $values['segapp_fields']['mapping']['rows'];
    foreach ($tmp_values as $key => $row) {
        if ($key == 'new') {
            if (!empty($values['segapp_fields']['mapping']['rows']['new']['drupal_field'])) {
                $drupal_field = $values['segapp_fields']['mapping']['rows']['new']['drupal_field'];
                $seg_field = $values['segapp_fields']['mapping']['rows']['new']['seg_field'];
                $mapped_values[$drupal_field] = $seg_field;
            }
            unset($values['segapp_fields']['mapping']['rows']['new']);
        }

        if ($row['delete'] != 1 && $key != 'new') {
            $mapped_values[$row['drupal_field']] = $row['seg_field'];
        }
    }
    $form_state['values']['segapp_fields'] = $mapped_values;

    $form_state['values']['segapp_page_tags'] = array_values(
        array_filter(
            $values['segapp_page_tags'], function ($var) {
                return !empty($var['page']);
            }
        )
    );

    $form_state['values']['segapp_url_initialize_tags'] = array_values(
        array_filter(
            $values['initialize_user'], function ($var) {
                return !empty($var['seg_field']);
            }
        )
    );
}

/**
 * Returns array of fields located on user account.
 */
function segapp_get_user_fields() 
{
    $user_fields = entity_get_all_property_info('user');

    $options = array('' => '-- Select --');
    foreach ($user_fields as $field => $field_info) {
        $options[$field] = $field_info['label'];
    }

    // Automatically map mail field.
    unset($options['mail']);

    return $options;
}
