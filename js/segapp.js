(function ($) {
    'use strict';

    /**
   * Update the summary for the module's vertical tab.
   */
    Drupal.behaviors.segAppTagSummary = {
        attach: function (context) {
            $('fieldset#edit-seg-tags', context).drupalSetSummary(
                function (context) {
                    if ($('#edit-seg-tags-tags', context).val() !== '') {
                        return Drupal.checkPlain($('#edit-seg-tags-tags', context).val());
                    }
                    else {
                        return Drupal.t('No Tags');
                    }
                }
            );
        }
    };

    Drupal.behaviors.segAppAutoComplete = {
        attach: function (context, settings) {
            function split(val) {
                return val.split(/,\s*/);
            }

            function extractLast(term) {
                return split(term).pop();
            }

            $('.tags-autocomplete', context)
            .on(
                'keydown', function (event) {
                    if (event.keyCode === $.ui.keyCode.TAB && $(this).autocomplete('instance').menu.active) {
                        event.preventDefault();
                    }
                }
            )
            .autocomplete(
                {
                    source: function (request, response) {
                        $.getJSON('/segapp/autocomplete', {term: extractLast(request.term)}, response);
                    },
                    search: function () {
                        var term = extractLast(this.value);
                        if (term.length < 2) {
                            return false;
                        }
                    },
                    focus: function () {
                        return false;
                    },
                    select: function (event, ui) {
                        var terms = split(this.value);
                        terms.pop();
                        terms.push(ui.item.value);
                        terms.push('');
                        this.value = terms.join(', ');
                        return false;
                    }
                }
            );
        }
    };

})(jQuery);
