(function ($) {
    'use strict';
    Drupal.behaviors.addSeg = {
        attach: function (context, settings) {
            if (typeof Drupal.settings.segapp.accountId !== 'undefined') {
                // Unique website identifier
                seg.config(
                    {
                        site: Drupal.settings.segapp.accountId
                    }
                );
            }
        }
    };

    Drupal.behaviors.segIdentify = {
        attach: function (context, settings) {
            if (typeof Drupal.settings.segapp.data !== 'undefined') {
                seg.identify(Drupal.settings.segapp.data);
            }
        }
    };

    Drupal.behaviors.segChangeEmail = {
        attach: function (context, settings) {
            if (typeof Drupal.settings.segapp.dataChange !== 'undefined') {
                seg.identify(Drupal.settings.segapp.dataChange);
            }
        }
    };

    Drupal.behaviors.segTrack = {
        attach: function (context, settings) {
            if (typeof Drupal.settings.segapp.page !== 'undefined') {
                Drupal.settings.segapp.page.forEach(
                    function (page) {
                        seg.track(page.type, page.data);
                    }
                );
            }
        }
    };
}(jQuery));
